//
//  Expense.m
//  ShouldModel
//
//  Created by Gonzalo Morales Asencio on 30/10/13.
//  Copyright (c) 2013 Gonzalo Morales Asencio. All rights reserved.
//

#import "Expense.h"
#import "User.h"


@implementation Expense

@dynamic amount;
@dynamic concept;
@dynamic dayOfmonth;
@dynamic isActive;
@dynamic periodicity;
@dynamic repeatEvery;
@dynamic typeOfTransaction;
@dynamic user;

@end
