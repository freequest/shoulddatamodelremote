//
//  User.m
//  ShouldModel
//
//  Created by Gonzalo Morales Asencio on 30/10/13.
//  Copyright (c) 2013 Gonzalo Morales Asencio. All rights reserved.
//

#import "User.h"
#import "Expense.h"


@implementation User

@dynamic name;
@dynamic photo;
@dynamic expenses;

@end
