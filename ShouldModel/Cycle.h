//
//  Cycle.h
//  ShouldModel
//
//  Created by Gonzalo Morales Asencio on 30/10/13.
//  Copyright (c) 2013 Gonzalo Morales Asencio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ScheduledExpense;

@interface Cycle : NSManagedObject

@property (nonatomic, retain) NSNumber * dailyAllowance;
@property (nonatomic, retain) NSNumber * daysToPayDay;
@property (nonatomic, retain) NSNumber * finalBalance;
@property (nonatomic, retain) NSNumber * naturalDays;
@property (nonatomic, retain) NSDate * nextPayDate;
@property (nonatomic, retain) NSNumber * payDay;
@property (nonatomic, retain) NSNumber * pendingNaturalDays;
@property (nonatomic, retain) NSNumber * pendingWeeks;
@property (nonatomic, retain) NSNumber * recomendation;
@property (nonatomic, retain) NSNumber * totalExpensesSinceToday;
@property (nonatomic, retain) NSSet *scheduledExpenses;
@end

@interface Cycle (CoreDataGeneratedAccessors)

- (void)addScheduledExpensesObject:(ScheduledExpense *)value;
- (void)removeScheduledExpensesObject:(ScheduledExpense *)value;
- (void)addScheduledExpenses:(NSSet *)values;
- (void)removeScheduledExpenses:(NSSet *)values;

@end
