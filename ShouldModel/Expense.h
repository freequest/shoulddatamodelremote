//
//  Expense.h
//  ShouldModel
//
//  Created by Gonzalo Morales Asencio on 30/10/13.
//  Copyright (c) 2013 Gonzalo Morales Asencio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Expense : NSManagedObject

@property (nonatomic, retain) NSNumber * amount;
@property (nonatomic, retain) NSString * concept;
@property (nonatomic, retain) NSNumber * dayOfmonth;
@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSString * periodicity;
@property (nonatomic, retain) NSString * repeatEvery;
@property (nonatomic, retain) NSString * typeOfTransaction;
@property (nonatomic, retain) User *user;

@end
