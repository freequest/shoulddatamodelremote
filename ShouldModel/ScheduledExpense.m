//
//  ScheduledExpense.m
//  ShouldModel
//
//  Created by Gonzalo Morales Asencio on 30/10/13.
//  Copyright (c) 2013 Gonzalo Morales Asencio. All rights reserved.
//

#import "ScheduledExpense.h"
#import "Cycle.h"


@implementation ScheduledExpense

@dynamic amount;
@dynamic concept;
@dynamic dayOfmonth;
@dynamic isActive;
@dynamic periodicity;
@dynamic repeatEvery;
@dynamic scheduledDate;
@dynamic typeOfTransaction;
@dynamic cycle;

@end
