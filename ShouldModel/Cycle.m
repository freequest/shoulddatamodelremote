//
//  Cycle.m
//  ShouldModel
//
//  Created by Gonzalo Morales Asencio on 30/10/13.
//  Copyright (c) 2013 Gonzalo Morales Asencio. All rights reserved.
//

#import "Cycle.h"
#import "ScheduledExpense.h"


@implementation Cycle

@dynamic dailyAllowance;
@dynamic daysToPayDay;
@dynamic finalBalance;
@dynamic naturalDays;
@dynamic nextPayDate;
@dynamic payDay;
@dynamic pendingNaturalDays;
@dynamic pendingWeeks;
@dynamic recomendation;
@dynamic totalExpensesSinceToday;
@dynamic scheduledExpenses;

@end
